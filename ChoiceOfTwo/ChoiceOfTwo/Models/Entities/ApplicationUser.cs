﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChoiceOfTwo.Models.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() { }

        /// <summary>
        /// Голосовал ли
        /// </summary>
        public bool isVote { get; set; }

        /// <summary>
        /// Выбор пользователя
        /// False - 0, True - 1
        /// </summary>
        public Nullable<bool> Choice { get; set; } 
    }
}