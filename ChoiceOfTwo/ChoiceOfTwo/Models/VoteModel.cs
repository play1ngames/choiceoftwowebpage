﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChoiceOfTwo.Models
{
    public class VoteModel
    {
        /// <summary>
        /// кол-во за первый
        /// </summary>
        public int First { get; set; }
        /// <summary>
        /// кол-во за второй
        /// </summary>
        public int Second { get; set; }
        /// <summary>
        /// кол-во не голосовали
        /// </summary>
        public int Third { get; set; }
        /// <summary>
        /// за что проголосовал юзер ; null - не головал
        /// </summary>
        public bool? CurrentUserChoice { get; set; } 
    }
}