﻿using ChoiceOfTwo.Models.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using ChoiceOfTwo.Models.Entities;
using ChoiceOfTwo.Models;

namespace ChoiceOfTwo.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Vote()
        {
            ApplicationUser user = await UserManager.FindByNameAsync(User.Identity.Name);
            VoteModel model = new VoteModel
            {
                CurrentUserChoice = null,
                First = UserManager.Users.Where(x => x.Choice.HasValue && x.Choice.Value == false).Count(),
                Second = UserManager.Users.Where(x => x.Choice.HasValue && x.Choice.Value == true).Count(),
                Third = UserManager.Users.Where(x => !x.Choice.HasValue).Count()
            };

            if (user != null)
            {
                model.CurrentUserChoice = user.Choice;
            }

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Vote(int choice)
        {
            ApplicationUser user = await UserManager.FindByNameAsync(User.Identity.Name);
            if (user != null )
            {
                user.isVote = true;
                user.Choice = (choice!=0);
                var result = await UserManager.UpdateAsync(user);
            }
            return RedirectToAction("Vote", "Home");
        }

    }
}